<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 11/13/2017
 * Time: 8:00 AM
 */
class Logs
{

     private  $PathLogs;
     private  $EnableDebug;
     private  $LogFileName;
     private  $timeStamp;

     public function SAVE_LOGS_TO($path){
         return $this->PathLogs  = $path;
     }
     /*boolean Varialbe and Result*/
     public function ENABLE_DEBUGGING_LOGS($boolean){
        return $this->EnableDebug = $boolean;
     }

     public function LOGS_FILENAME($filename){
         return $this->LogFileName = $filename;
     }

    private function IsExisting(){
         if(!file_exists($this->PathLogs)){
             /* The Folder/Directory is missing or not Existing
             */
             $this->MakeDir();
            return true;
         }else{
             /* The File/Directory is Existing*/
             return true;
         }
    }
    private function MakeDir(){
        mkdir($this->PathLogs, 0777,true);
    }
    public function setTimestamp($format) {
        $this->timeStamp = date($format)." &raquo; ";
    }

    public function CREATE_LOGS($insert){
        if(isset($this->timeStamp)){
            if($this->IsExisting() == true && $this->EnableDebug == true){
                file_put_contents($this->LogFileName,$this->timeStamp." ".$insert."<br>",FILE_APPEND);

            }else{
                trigger_error('Logs Directory is needed Permission!',"E_USER_WARNING");
            }
        }else{
            trigger_error('Invalid DateTime Format for logs or DateTime not set yet.',"E_USER_WARNING");
        }
    }

    public function GET_LOGS(){
       $content = fopen($this->LogFileName,"r") or die("Unable to Open File!");
       if (filesize($this->LogFileName) != 0){
           print fread($content,filesize($this->LogFileName));
           fclose($content);
       }else{
           return 0;
       }
    }



}
