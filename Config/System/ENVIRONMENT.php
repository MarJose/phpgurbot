<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 8/24/2018
 * Time: 3:43 PM
 */
/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 */
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
class ENVIRONMENT
{
    /**
     * @param $environment
     */
    public function apply($environment){

        /** @var TYPE_NAME $environment */
        switch ($environment){
                case 'development':
                    error_reporting(-1);
                    ini_set('display_errors', 1);
                    break;
                case 'testing':
                case 'production':
                    ini_set('display_errors', 0);
                    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
                    break;
                default:
                    header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
                    echo 'The application environment is not set correctly.';
                    echo "</br> [+] development ";
                    echo "</br> [+] testing";
                    echo "</br> [+] production";
                    exit(1); // EXIT_ERROR
            }

    }
}