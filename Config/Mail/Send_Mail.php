<?php

class Send_Mail
{
    /* Mail Recipient*/
    private static $To;
    /* Mail Message Subject */
    private static $Subject;
    /* Mail Message to the Recipients */
    private static $Message;
    /* Instead of example@gmail.com it will be "John Pawnshop" */
    private static $DisplayName;
    /* Gmail Account is Need to Send Mail */
    /* Gmail Username Example@gmail.com */
    private static $Gmail_Username;
    /* Gmail Password */
    private static $Gmail_Password;
    /*@var boolean*/
    private static $sent;

    public static function Send_Mail_to($to)
    {
        self::$To = $to;
    }

    public static function Send_Mail_subject($subject)
    {
        self::$Subject = $subject;
    }

    public static function Send_Mail_message($message)
    {
        self::$Message = $message;
    }

    public static function Gmail_Username($user)
    {
        self::$Gmail_Username = $user;
    }

    public static function Gmail_Password($pass)
    {
        self::$Gmail_Password = $pass;
    }

    public static function Display_Name($DisplayName)
    {
        self::$DisplayName = $DisplayName;
    }

    public static function SendNow()
    {
        $phpmailer = new PHPMailer();
        $phpmailer->IsSMTP(true);
        $phpmailer->IsHTML(true);
        $phpmailer->SMTPAuth = true;
        $phpmailer->Port = 465;
        $phpmailer->SetFrom(self::$Gmail_Username, self::$DisplayName);
        $phpmailer->AddReplyTo(self::$Gmail_Username, self::$DisplayName);
        $phpmailer->Subject = self::$Subject;
        $phpmailer->MsgHTML(self::$Message);
        $phpmailer->AddAddress(self::$To);
        self::$sent = $phpmailer->Send();
    }

    public function IsSend(){
        if(self::$sent == true){
            return true;
        }else{
            return false;
        }
    }

}
