<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 11/9/2017
 * Time: 8:01 AM
 */

class Connection
{

    private  $hostname;
    private  $dbname;
    private  $dbusername;
    private  $dbpassword;
    private  $cont = null;

    function DBHost($host){
        return $this->hostname = $host;
    }

    function DBName($dbname){
        return $this->dbname = $dbname;
    }

    function DBUsername($dbuser){
        return $this->dbusername = $dbuser;
    }

    function DBPassword($dbpass){
        return $this->dbpassword = $dbpass;
    }


    function ConnectToDB(){
        //Memcache


        // One connection through whole application
        if ( null == $this->cont )
        {
            try
            {
                $this->cont = new PDO("mysql:host=".$this->hostname.";"."dbname=".$this->dbname, $this->dbusername, $this->dbpassword, array(PDO::ATTR_PERSISTENT => true));
            }
            catch(PDOException $e)
            {
                die($e->getMessage());
            }
        }
        return $this->cont;
    }

    function DisconnectTODB()
    {
       $this->cont = null;
    }


}