<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 11/11/2017
 * Time: 11:49 AM
 */

class DB
{

    /*@var string MYSQL Bin Path*/
    private static $mysqlBinPath;

    /*Backup Destination file to be saved*/
    private static $destination; // Example: C:\wamp64\my_php_SystemFile_\Backup_DB
    private static $hostname;
    private static $dbname;
    private static $dbusername;
    private static $dbpassword;

    /* boolean value */
    private static $IsBackup;


    public function MYSQL_Bin_Path($path){
        self::$mysqlBinPath = $path;
    }

    public function MYSQL_DB_TO_SAVE($path){
        self::$destination = $path;
    }

    public function DB_HOSTNAME($host){
        self::$hostname = $host;
    }
    public function DB_NAME($name){
        self::$dbname = $name;
    }
    public function DB_USER($user){
        self::$dbusername = $user;
    }
    public function DB_PASS($pass){
        self::$dbpassword = $pass;
    }

    /* int 1-0 Result*/
    public function IsBackup(){
        return self::$IsBackup;
    }

    public function Backup_DB(){
        /* Folder Search if it is Exisitng!*/
        /* if Folder is not Existing then Make a Folder */
        if (!file_exists(self::$destination)) {
            mkdir(self::$destination, 0777, true);
        }

        /* External Command Using CMD */
        $command = exec(self::$mysqlBinPath.'\mysqldump --opt --user='.self::$dbusername.' --password='.self::$dbpassword.' --host='.self::$hostname.' '.self::$dbname.' > '.self::$destination.'\MAICO-DB-Backup-'.date('F-d-Y_h-i-A').'.sql');
        if(isset($command)){
            self::$IsBackup = 1;
        }else{
            self::$IsBackup = 0;
        }

    }

}
?>