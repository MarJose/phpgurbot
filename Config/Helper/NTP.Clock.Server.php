<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 9/18/2018
 * Time: 10:41 AM
 */

class NTP_Clock_Server
{
    private $ntpServer;
    private $ntpPort;
    private $timercvd;
    private $ntpTimeValue;
    private $ntpServerTimeStamp;
    private $ntpErrorCode;
    private $ntpErrorMessage;
    private $ntpTimeout = 5;


    /**
     * @param $string
     * i.e ntp.pagasa.dost.gov.ph -> for philippines country only
     */
    public function  set_NTPServer($string){
        $this->ntpServer = $string;
    }

    /**
     * default port of Network Time Protocol is 37
     * @param $int
     */
    public function  set_NTPPort($int){
        $this->ntpPort = $int;
    }

    public function get_NTPServer_CurrentTime_Array(){
        return $this->query_time_server();
    }

    /**
     * @return false|string
     */
    public function get_NTPServer_CurrentTime(){
       $this->timercvd = $this->query_time_server();

       if(!$this->timercvd[1]){
           $this->ntpTimeValue =  bin2hex($this->timercvd[0]);
           $this->ntpTimeValue = abs(HexDec('7fffffff') - HexDec($this->ntpTimeValue) - HexDec('7fffffff'));
           $this->ntpServerTimeStamp =  $this->ntpTimeValue - 2208988800;

       }
        return date("h:i:s A",  $this->ntpServerTimeStamp);

    }

    /**
     * @return false|string
     */
    public function get_NTPServer_CurrentDate(){
        $this->timercvd = $this->query_time_server();

        if(!$this->timercvd[1]){
            $this->ntpTimeValue =  bin2hex($this->timercvd[0]);
            $this->ntpTimeValue = abs(HexDec('7fffffff') - HexDec($this->ntpTimeValue) - HexDec('7fffffff'));
            $this->ntpServerTimeStamp =  $this->ntpTimeValue - 2208988800;

        }
        return date("d/m/Y",  $this->ntpServerTimeStamp);
    }


    /**
     * @return array
     */
    private function query_time_server(){
        $fp = fsockopen($this->ntpServer,$this->ntpPort,$this->ntpErrorCode,$this->ntpErrorMessage,$this->ntpTimeout);
        # parameters: server, socket, error code, error text, timeout
        if($fp)
        {
            fputs($fp, "\n");
            $timevalue = fread($fp, 49);
            fclose($fp); # close the connection
        }
        else
        {
            $timevalue = " ";
        }

        $ret = array();
        $ret[] = $timevalue;
        if (!empty($this->ntpErrorCode)) {
            $ret[] = $this->ntpErrorCode;
        }     # error code
        if (!empty($this->ntpErrorMessage)) {
            $ret[] = $this->ntpErrorMessage;
        }  # error text
        return($ret);
    }


}