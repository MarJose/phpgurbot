<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 8/29/2018
 * Time: 11:38 AM
 */

class Session
{


    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
    }

    /**
    * Write the session data
     */
    function sess_write($key, $data){
        $_SESSION[$key] = $data;
    }


    /**
    * Get Data from the session storage
     **/

    function sess_getData($key){
        return $_SESSION[$key];
    }



    /**
    Get all session data in array format
     */

    function sess_getAll(){
        return $_SESSION;
    }

    function sess_destroy(){
        session_unset();
        session_destroy();
    }




}