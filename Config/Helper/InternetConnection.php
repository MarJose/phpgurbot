<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 9/18/2018
 * Time: 10:26 AM
 */

class InternetConnection
{
   private $host = "www.google.com";
   private $port = 80;
   private $is_conn = false;
   private $connected = null;


    /**
     * @return bool
     */
    function isConnected(){
        $this->connected = @fsockopen($this->host, $this->port);
        if($this->connected){
            $this->is_conn = true;
            fclose($this->connected);
        }else{
            $this->is_conn = false;
        }
        return $this->is_conn;
    }

}