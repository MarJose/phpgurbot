<?php

/**
 * @param $string
 */
class XSSProtection{


    /**
     * @param $string
     * @return string
     */
    public function xss_output_clean($string)
    {
        return htmlentities($string,ENT_QUOTES, 'UTF-8');
    }


}

