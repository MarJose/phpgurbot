<?php
/**
 * Created by PhpStorm.
 * User: MarJose
 * Date: 11/9/2017
 * Time: 8:10 AM
 */

/*Check PHP Version if it is Lower than the Requirements Needs*/
$ver = (float)phpversion();
if (version_compare($ver, '7.0.0', '<') )
    exit("Sorry, this version of PHP Project will only run on PHP version 7.0.0 or greater!\n");

//Include classes and functions
require 'Database/Connection.php';
require 'Database/DB.php';
require 'Fetcher/Browser.php';
require 'Fetcher/ErrorLogs.php';
require 'Helper/Session.php';
require 'Helper/XSSProtection.php';
require 'Helper/InternetConnection.php';
require 'Helper/NTP.Clock.Server.php';
require 'Mail/Send_Mail.php';
require 'Security/Common.php';
require 'Security/CSRF_Firewall.php';
require 'Security/hash.php';
require 'Security/mbstring.php';
require 'Security/Password.php';
require 'Security/standard.php';
require 'System/ENVIRONMENT.php';



/*Environment*/
$env = new ENVIRONMENT();
$env->apply('development');

/*Path where the log.txt will be save during debugging is enable*/
$logs = new Logs();
$logs->SAVE_LOGS_TO('../../');
$logs->LOGS_FILENAME('logs.txt');
$logs->setTimestamp("D M d 'y h.i A");
$logs->ENABLE_DEBUGGING_LOGS(true);
//create and write logs
//$logs->CREATE_LOGS('');

$NTPClock = new NTP_Clock_Server();
$NTPClock->set_NTPServer('ntp.pagasa.dost.gov.ph');
$NTPClock->set_NTPPort(37);


/*Setting up Connection to the Database*/
$conn = new Connection();
$conn->DBHost('localhost');
$conn->DBName('');
$conn->DBUsername('');
$conn->DBPassword('');
//$conn->ConnectToDB();


/*Setting up Mailer*/
Send_Mail::Gmail_Password('');
Send_Mail::Gmail_Username('');
Send_Mail::Display_Name('');


/*Setting up for Backing up Database*/

//XAMMP: C:\xampp\mysql\bin
//WAMP: c:\wamp\bin\mysql\mysql$version\bin

$db_backup = new DB();
$db_backup->DB_HOSTNAME('localhost');
$db_backup->DB_NAME('');
$db_backup->DB_USER('');
$db_backup->DB_PASS('');
$db_backup->MYSQL_DB_TO_SAVE(__DIR__.'../backup-db');
$db_backup->MYSQL_Bin_Path('C:\xampp\mysql\bin');
//$db->Backup_DB();

/*Anti-CSRF*/
$csrf = new CSRF_Firewall();

/*Session Helper*/
$session = new Session();

