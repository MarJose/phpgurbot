# PHPGurbot

[![Build Status](https://img.shields.io/badge/build-passed-brightgreen.svg)](https://gitlab.com/whoami213/phpgurbot) [![License Status](https://img.shields.io/badge/License-MIT-red.svg)](https://gitlab.com/whoami213/phpgurbot/blob/master/LICENSE)


PHPGurbot is a Semi-Model-View-Controller PHP Framework, and added with the most CSS Framework,JS Framework and JS Alert Framework that is used to make a site awesome.

##### Assets/Vendors Include:
###### CSS Framework:
  - Bootstrap 4.1.3
  - Bulma 0.7.2
  - Zurd Foundation 6
  - Semantic UI 2.4.1

###### JS Framework:
  - React
  - VueJS

###### JS Alert/Notification Framework:
  - alertify
  - bootbox
  - sweetalert2
  - toastr


You can also:
  - Import other frameworks or other vendors and etc.


PHPGurbot is a semi-mvc php framework that make easier for those who don't use any MVC PHP Framework bcoz PHPGurbot is a ready for the hard-coder programmer.


> Added new Password Encryption and Verficaion syntax

> Easily to configure connection to the database with PDO

### Installation
```sh
git clone https://gitlab.com/whoami213/phpgurbot.git
```
or
download it using browser
```
https://gitlab.com/whoami213/phpgurbot/-/archive/master/phpgurbot-master.zip
```
### Plugins

PHPGurbot is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | Links |
| ------ | ------ |
| DataTables | [https://datatables.net/]|
| PHPExcel | [https://about:blank]|


### Development

Want to contribute? Great!

### Todos

 - Write MORE Tests
 - Add Attack Protection

License
----

[![License Status](https://img.shields.io/badge/License-MIT-red.svg)](https://gitlab.com/whoami213/phpgurbot/blob/master/LICENSE)


